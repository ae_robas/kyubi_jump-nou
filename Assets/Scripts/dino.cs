﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody2D))]
public class dino : MonoBehaviour
{
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    public float maxS = 11f;
    private bool jump;
    private bool attack;
    public float fuerzaSalto = 5.0f;

    public float bulletCooldown = 1.0f;
    public float bulletSpeed = 15.0f;
    private float bulletTimer = 0.0f;
    public float powerUpInvencibilityTime = 2.0f;
    private float powerUpTimer = 0.0f;
    [SerializeField] private GameObject graphics = null;
    [SerializeField] private Animator animator = null;
    [SerializeField] private GameObject bullet = null;

    private GameObject lastBullet;
    private bool bulletSpeedSet = false;
    public bool isGrounded = false;

    public bool isPoweredUp = false;
    private bool lostPowerUp = false;
    public bool isDead = false;
    private float scalaActual;
    public float tmp=0;
    public float segundosespera=2;

    //[SerializeField] AudioSource audio;

    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    
    void FixedUpdate()
    {
        if (isDead) 
        { 
            return; 
        }

        rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);
        rb2d.AddForce(Vector2.right * move * maxS);

        if (jump && isGrounded)
        {
            //Debug.LogError("Salta");
            jump = false;
            rb2d.velocity = Vector2.zero;
            rb2d.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);
        }

        if (move > 0)
        {
            //graphics.transform.localScale = new Vector3(graphics.transform.localScale.x, graphics.transform.localScale.y, graphics.transform.localScale.z);
            graphics.GetComponent<SpriteRenderer>().flipX = false;
        }
        else if (move < 0)
        {
            //graphics.transform.localScale = new Vector3(-graphics.transform.localScale.x, graphics.transform.localScale.y, graphics.transform.localScale.z);
            graphics.GetComponent<SpriteRenderer>().flipX = true;
        }

        animator.SetInteger("Velocidad", (int)Math.Abs(rb2d.velocity.x));

        if(lastBullet != null && !bulletSpeedSet){
            lastBullet.GetComponent<Rigidbody2D>().velocity = bulletSpeed * (graphics.GetComponent<SpriteRenderer>().flipX ? Vector2.left : Vector2.right);
            bulletSpeedSet = true;
        }
    }

    private void Update()
    {
        if (isDead) 
        {
            tmp += 1 * Time.deltaTime;
            if(tmp >= segundosespera)
            {
               SceneManager.LoadScene("Muerte");
            }
            return; 
        }
        
        move = Input.GetAxis("Horizontal");
        jump = Input.GetKey(KeyCode.Space);
        attack = Input.GetButton("Fire1");

        //if (attack && isGrounded)
        // {
        //     move = 0f;
        //    jump = false;
        //    animator.SetBool("attack", attack);
        //}
        //else
        //{
        //    animator.SetBool("attack", false);
        //}

        // POWER UP

        if (isPoweredUp){

            if (bulletTimer >= bulletCooldown)
            {
                Destroy(lastBullet);

                if (attack)
                {
                    
                    lastBullet = Instantiate(bullet, rb2d.transform.position, Quaternion.identity);
                    bulletSpeedSet = false;
                    bulletTimer = 0.0f;
                }
            }

            bulletTimer += Time.deltaTime;
        }

        if (lostPowerUp)
        {
            powerUpTimer += Time.deltaTime;
            if(powerUpTimer >= powerUpInvencibilityTime)
            {
               lostPowerUp = false;
               powerUpTimer = 0.0f;
            }
            return; 
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.gameObject.CompareTag("rangeCollider") &&
        !collision.gameObject.CompareTag("PowerUp"))
        {
            isGrounded = true;
            
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!collision.gameObject.CompareTag("rangeCollider") &&
        !collision.gameObject.CompareTag("PowerUp"))
        {
            isGrounded = false;
            
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Plataforma")
        {
            rb2d.velocity = new Vector2(0, 0);
            this.transform.parent = collision.transform;
        }
        
        else if(collision.gameObject.CompareTag("enemy")){
            if (isPoweredUp)
            {
                isPoweredUp = false;
                lostPowerUp = true;
            } else if (!lostPowerUp) {
                Debug.LogError("muerto");
                //enemigo inmobil
                isDead = true;
                animator.SetTrigger("Dead");
            }
        }
        else if(collision.gameObject.CompareTag("deathFloor"))
        {
            Debug.LogError("muerto");
            //enemigo inmobil
            isDead = true;
            animator.SetTrigger("Dead");
        }
        else if(collision.gameObject.CompareTag("saltante"))
        {
            rb2d.AddForce(Vector2.up * move * maxS * 4);
        }
        
        
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Plataforma")
        {
            this.transform.parent = null;
        }
    }
}




