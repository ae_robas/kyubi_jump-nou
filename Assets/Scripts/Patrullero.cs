﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrullero : MonoBehaviour
{
    [SerializeField] Transform[] posiciones;
    [SerializeField] int posactual = 0;
    [SerializeField] float velocidadEnemigo = 1.0f;

    private void Update()
    {
        if (Mathf.Abs(Vector2.Distance(posiciones[posactual].position, transform.position)) < 0.1f)
        {
            posactual++;

            /*
            if (posactual >= posiciones.Length)
            {
                posactual = 0;
            }
            */

            posactual %= posiciones.Length;
        }

        transform.position = Vector2.MoveTowards(transform.position, posiciones[posactual].position, Time.deltaTime * velocidadEnemigo);
        if (posactual==1)
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        } else {
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            /*
            if(transform.position.y < collision.transform.position.y) // COORDENADAS INCORRECTAS
            {
                Destroy(gameObject);
            }
            */
        }
                
    }
}
