﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyRangeController : MonoBehaviour
{
    [SerializeField] Transform player;
    public Action startTrack;
    bool isTracking = false;
    [SerializeField] float velocidadEnemigo = 1.0f;

    void Awake(){
        startTrack += startTracking;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isTracking)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.position, Time.deltaTime * velocidadEnemigo);
        }
    }

    void startTracking(){
        isTracking = true;
    }
}
