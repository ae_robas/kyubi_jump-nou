﻿using System.Collections;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

    //[SerializeField] AudioSource audioSource;

    public void PulsaPlay()
    {
        Debug.LogError("He pulsado Play");


       // gameObject.GetComponent<AudioSource>().Play();


        SceneManager.LoadScene("kyubi");
    }
    public void PulsaOptions()
    {
        Debug.LogError("He pulsado Options");

        SceneManager.LoadScene("Options");
    }

    public void PulsaCredits()
    {
        Debug.LogError("He pulsado Credits");

        //gameObject.GetComponent<AudioSource>().Play();

        SceneManager.LoadScene("Credits");
    }


    public void PulsaExit()
    {
        //gameObject.GetComponent<AudioSource>().Play();
        Application.Quit();
    }
    public void PulsaBacktoMenu()
    {
        Debug.LogError("He pulsado PulsaBacktoMenu");

        //gameObject.GetComponent<AudioSource>().Play();

        SceneManager.LoadScene("Title");
    }


}

